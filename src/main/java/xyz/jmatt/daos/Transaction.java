package xyz.jmatt.daos;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Transaction {
//    private final String URL = "jdbc:sqlite:" + "server" + File.separator + "db" + File.separator + "database.sqlite"; //local testing
    private final String URL = "jdbc:sqlite:" + "FoodInventory" + File.separator + "FoodInventory_Server" + File.separator + "db" + File.separator + "database.sqlite"; //pi
    private Connection connection;

    /**
     * Creates a new Transaction
     * @throws SQLException thrown if the class could not connect to the database
     */
    public Transaction() throws SQLException {
        try {
            Class.forName("org.sqlite.JDBC").newInstance();
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("ERROR: Could not load sqlite .jar");
        }
        connection = DriverManager.getConnection(URL);
        connection.setAutoCommit(false);
    }

    /**
     * Gets the active connection for the transaction
     * @return the connection
     */
    public Connection getConnection() {
        return connection;
    }

    /**
     * Rolls back the current connection to the database
     */
    public void rollback() {
        try {
            connection.rollback();
        } catch (SQLException e) {
            System.err.println("ERROR: Could not rollback database transaction");
        }
    }

    /**
     * Commits any pending changes to the database
     * @throws SQLException thrown if the changes could not be committed
     */
    public void commit() throws SQLException {
        connection.commit();
    }

    /**
     * Closes the connection associated with the transaction
     */
    public void close() {
        try {
            connection.close();
            connection = null;
        } catch (SQLException e) { //unable to close connection
            System.err.println("ERROR: Could not close connection to database");
        }
    }

}
