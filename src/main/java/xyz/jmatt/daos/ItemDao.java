package xyz.jmatt.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import xyz.jmatt.models.ItemModel;

public class ItemDao {
    private Connection connection;

    public ItemDao(Transaction transaction) {
        this.connection = transaction.getConnection();
    }

    public void pushNewItem(ItemModel model) throws SQLException {
        PreparedStatement prep = connection.prepareStatement(
                "insert into Items (barcode,quantity,last_added,title,image,expiration) values(?,?,?,?,?,?);");
        prep.setString(1, model.getBarcode());
        prep.setInt(2, 1);
        prep.setLong(3, System.currentTimeMillis());
        prep.setString(4, model.getTitle());
        prep.setString(5, model.getImage());
        prep.setLong(6, model.getExpiration());
        prep.executeUpdate();
        prep.close();
    }

    public void incrementItemQuantity(String barcode) throws SQLException {
        PreparedStatement prep = connection.prepareStatement(
                "update Items set quantity = quantity + 1 where barcode = ?");
        prep.setString(1, barcode);
        prep.executeUpdate();
        prep.close();
        PreparedStatement prepTime = connection.prepareStatement(
                "update Items set last_added = ? where barcode = ?");
        prepTime.setLong(1, System.currentTimeMillis());
        prepTime.setString(2, barcode);
        prepTime.executeUpdate();
        prepTime.close();
    }

    public void decrementItemQuantity(String barcode) throws SQLException {
        ItemModel item = getItem(barcode);
        if(item == null) {
            return;
        }
        if(item.getQuantity() > 0) {
            PreparedStatement prep = connection.prepareStatement(
                    "update Items set quantity = quantity - 1 where barcode = ?");
            prep.setString(1, barcode);
            prep.executeUpdate();
            prep.close();
            PreparedStatement prepTime = connection.prepareStatement(
                    "update Items set last_added = ? where barcode = ?");
            prepTime.setLong(1, System.currentTimeMillis());
            prepTime.setString(2, barcode);
            prepTime.executeUpdate();
            prepTime.close();
        }
    }

    public boolean doesItemExist(String barcode) throws SQLException {
        PreparedStatement prep = connection.prepareStatement(
                "select * from Items where barcode = ?;");
        prep.setString(1, barcode);
        ResultSet resultSet = prep.executeQuery();

        if(!resultSet.next()) {
            resultSet.close();
            prep.close();
            return false;
        }
        resultSet.close();
        prep.close();
        return true;
    }

    public ItemModel getItem(String barcode) throws SQLException {
        PreparedStatement prep = connection.prepareStatement(
                "select * from Items where barcode = ?;");
        prep.setString(1, barcode);
        ResultSet resultSet = prep.executeQuery();

        ItemModel result = null;
        if(resultSet.next()) {
            result = new ItemModel();
            result.setTitle(resultSet.getString("title"));
            result.setBarcode(barcode);
            result.setImage(resultSet.getString("image"));
            result.setQuantity(resultSet.getInt("quantity"));
            result.setExpiration(resultSet.getLong("expiration"));
        }
        resultSet.close();
        prep.close();
        return result;
    }

    public ItemModel getRecentItems() throws SQLException {
        PreparedStatement prep = connection.prepareStatement(
                "select * from (select * from Items order by last_added desc limit 10) order by last_added desc");
        ResultSet resultSet = prep.executeQuery();

        List<ItemModel> items = new ArrayList<>();

        while(resultSet.next()) {
            ItemModel item = new ItemModel();
            item.setBarcode(resultSet.getString("barcode"));
            item.setLastAdded(resultSet.getLong("last_added"));
            item.setTitle(resultSet.getString("title"));
            item.setImage(resultSet.getString("image"));
            item.setQuantity(resultSet.getInt("quantity"));
            item.setExpiration(resultSet.getLong("expiration"));
            items.add(item);
        }
        resultSet.close();
        prep.close();

        ItemModel result = new ItemModel();
        result.setItems(items);
        return result;
    }

    public ItemModel getAllItems() throws SQLException {
        PreparedStatement prep = connection.prepareStatement(
                "select * from Items;");
        ResultSet resultSet = prep.executeQuery();

        List<ItemModel> items = new ArrayList<>();

        while(resultSet.next()) {
            ItemModel item = new ItemModel();
            item.setBarcode(resultSet.getString("barcode"));
            item.setLastAdded(resultSet.getLong("last_added"));
            item.setTitle(resultSet.getString("title"));
            item.setImage(resultSet.getString("image"));
            item.setQuantity(resultSet.getInt("quantity"));
            item.setExpiration(resultSet.getLong("expiration"));
            items.add(item);
        }
        resultSet.close();
        prep.close();

        ItemModel result = new ItemModel();
        result.setItems(items);
        return result;
    }

    public void updateItem(ItemModel itemModel) throws SQLException {
        PreparedStatement prep = connection.prepareStatement(
                "update Items set title = ?, quantity = ?, last_added = ?, image = ?, expiration = ? where barcode = ?;");
        prep.setString(1, itemModel.getTitle());
        prep.setInt(2, itemModel.getQuantity());
        prep.setLong(3, itemModel.getLastAdded());
        prep.setString(4, itemModel.getImage());
        prep.setLong(5, itemModel.getExpiration());
        prep.setString(6, itemModel.getBarcode());

        prep.executeUpdate();

        prep.close();
    }
}
