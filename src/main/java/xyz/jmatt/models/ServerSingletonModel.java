package xyz.jmatt.models;

public class ServerSingletonModel {
    private static final ServerSingletonModel INSTANCE = new ServerSingletonModel();
    private String scanState = "in";

    public static ServerSingletonModel getInstance() {
        return INSTANCE;
    }

    public String getScanState() {
        return scanState;
    }

    public void setScanState(String scanState) {
        this.scanState = scanState;
    }
}
