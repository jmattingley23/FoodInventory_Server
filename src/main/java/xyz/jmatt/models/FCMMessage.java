package xyz.jmatt.models;

public class FCMMessage {
    private String to;
    private FCMMessageData data;

    public FCMMessage(String to, String data) {
        this.to = to;
        this.data = new FCMMessageData(data);
    }
}

class FCMMessageData {
    private String message = "";

    FCMMessageData(String data) {
        message = data;
    }
}
