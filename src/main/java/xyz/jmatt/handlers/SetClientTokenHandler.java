package xyz.jmatt.handlers;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;

import java.io.IOException;
import java.net.HttpURLConnection;

import xyz.jmatt.results.SimpleResult;
import xyz.jmatt.services.SetClientTokenService;

public class SetClientTokenHandler extends AbstractHandler {
    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        try {
            if(httpExchange.getRequestMethod().toLowerCase().equals("post")) { //must be post
                String[] requesterURLStr = httpExchange.getRequestURI().toString().split("/");

                System.out.println("updating stored client token...");
                SetClientTokenService service = new SetClientTokenService();
                service.setClientToken(requesterURLStr[2]);

                SimpleResult result = new SimpleResult("", false);
                sendGoodResponse(httpExchange, new Gson().toJson(result, SimpleResult.class));
            } else {
                httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, 0);
            }
        } catch (IOException e) {
            sendBadResponse(httpExchange, new Gson().toJson(new SimpleResult("ERROR: Bad request", true), SimpleResult.class));
        }
    }
}
