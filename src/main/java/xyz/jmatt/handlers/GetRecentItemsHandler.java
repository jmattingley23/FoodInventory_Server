package xyz.jmatt.handlers;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;

import java.io.IOException;
import java.net.HttpURLConnection;

import xyz.jmatt.models.ItemModel;
import xyz.jmatt.services.GetRecentItemsService;

public class GetRecentItemsHandler extends AbstractHandler {
    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        try {
            if(httpExchange.getRequestMethod().toLowerCase().equals("get")) { //must be a get
                GetRecentItemsService service = new GetRecentItemsService(); //new associated service
                ItemModel result = service.getRecentItems(); //grab the most recent items from the db
                sendGoodResponse(httpExchange, new Gson().toJson(result, ItemModel.class)); //send data as json
            } else {
                httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, 0);
            }
        } catch (IOException e) {
            httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_SERVER_ERROR, 0);
        }
    }
}
