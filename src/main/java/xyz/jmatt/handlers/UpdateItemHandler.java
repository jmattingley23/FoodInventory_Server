package xyz.jmatt.handlers;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;

import xyz.jmatt.models.ItemModel;
import xyz.jmatt.results.SimpleResult;
import xyz.jmatt.services.UpdateItemService;

public class UpdateItemHandler extends AbstractHandler {
    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        try {
            if(httpExchange.getRequestMethod().toLowerCase().equals("post")) {
                InputStream reqBody = httpExchange.getRequestBody();
                String reqData = readString(reqBody);
                ItemModel model = new Gson().fromJson(reqData, ItemModel.class);

                UpdateItemService service = new UpdateItemService();
                SimpleResult result = service.updateItem(model);
                sendGoodResponse(httpExchange, new Gson().toJson(result, SimpleResult.class));
            } else {
                httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, 0);
            }
        } catch (IOException e) {
            httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_SERVER_ERROR, 0);
        }
    }
}
