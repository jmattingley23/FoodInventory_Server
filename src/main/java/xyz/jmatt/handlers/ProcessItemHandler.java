package xyz.jmatt.handlers;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;

import java.io.IOException;

import xyz.jmatt.results.SimpleResult;
import xyz.jmatt.services.ProcessItemService;

public class ProcessItemHandler extends AbstractHandler {
    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        try {
            if(httpExchange.getRequestMethod().toLowerCase().equals("post")) { //make sure its a post
                String[] requestedURLStr = httpExchange.getRequestURI().toString().split("/");

                ProcessItemService processItemService = new ProcessItemService(); //new service
                SimpleResult result = processItemService.processItem(requestedURLStr[2]); //isolate the barcode & send to service

                sendGoodResponse(httpExchange, new Gson().toJson(result, SimpleResult.class));
            } else { //was not a post, send bad request
                SimpleResult result = new SimpleResult("ERROR: Http request method must be POST", true);
                sendBadResponse(httpExchange, new Gson().toJson(result, SimpleResult.class));
            }
        } catch (IOException e) {
            SimpleResult result = new SimpleResult("ERROR: Http request method must be POST", true);
            sendBadResponse(httpExchange, new Gson().toJson(result, SimpleResult.class));
        }
    }
}