package xyz.jmatt.handlers;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;

import java.io.IOException;
import java.net.HttpURLConnection;

import xyz.jmatt.results.SimpleResult;
import xyz.jmatt.services.SetScanService;

public class SetScanHandler extends AbstractHandler {
    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        try {
            if(httpExchange.getRequestMethod().toLowerCase().equals("post")) { //must be a post
                String[] requestedURLStr = httpExchange.getRequestURI().toString().split("/");

                SetScanService service = new SetScanService(); //new service
                service.setScanState(requestedURLStr[2]); //get requested state and send to service

                SimpleResult result = new SimpleResult("", false); //no errors
                sendGoodResponse(httpExchange, new Gson().toJson(result, SimpleResult.class));
            } else {
                httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, 0);
            }
        } catch (IOException e) {
            httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_SERVER_ERROR, 0);
        }
    }
}
