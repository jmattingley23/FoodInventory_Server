package xyz.jmatt.handlers;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;

import java.io.IOException;
import java.net.HttpURLConnection;

import xyz.jmatt.models.ItemModel;
import xyz.jmatt.services.GetAllItemsService;

public class GetAllItemsHandler extends AbstractHandler {
    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        try {
            if(httpExchange.getRequestMethod().toLowerCase().equals("get")) { //must get get
                GetAllItemsService service = new GetAllItemsService(); //new associated service
                ItemModel result = service.getItems(); //grab items from db
                sendGoodResponse(httpExchange, new Gson().toJson(result, ItemModel.class));
            } else {
                httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, 0);
            }
        } catch (IOException e) {
            httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_SERVER_ERROR, 0);
        }
    }
}
