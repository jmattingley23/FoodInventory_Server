package xyz.jmatt.handlers;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;

import java.io.IOException;
import java.net.HttpURLConnection;

import xyz.jmatt.results.SimpleResult;
import xyz.jmatt.services.GetScanService;

public class GetScanHandler extends AbstractHandler {
    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        try {
            if(httpExchange.getRequestMethod().toLowerCase().equals("get")) { //must be a get
                GetScanService service = new GetScanService(); //new service
                SimpleResult result = new SimpleResult(service.getScanState(), false); //grab the scanner state
                sendGoodResponse(httpExchange, new Gson().toJson(result, SimpleResult.class)); //send result back
            } else {
                httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, 0);
            }
        } catch (IOException e) {
            httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_SERVER_ERROR, 0);
        }
    }
}
