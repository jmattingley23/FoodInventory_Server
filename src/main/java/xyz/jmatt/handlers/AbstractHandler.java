package xyz.jmatt.handlers;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.net.HttpURLConnection;

/**
 * Abstract class for all handlers
 */
public abstract class AbstractHandler implements HttpHandler {
    /**
     * writes the given response string to the body and sends HTTP_OK headers
     * @param httpExchange the exchange
     * @param response the response to write
     * @throws IOException thrown if the response could not be written
     */
    protected void sendGoodResponse(HttpExchange httpExchange, String response) throws IOException {
        httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);
        OutputStream body = httpExchange.getResponseBody();
        writeString(response, body);
        body.close();
    }

    /**
     * writes the given response string to the body and sends HTTP_BAD_REQUEST headers
     * @param httpExchange the exchange
     * @param response the response to write
     * @throws IOException thrown if the response could not be written
     */
    protected void sendBadResponse(HttpExchange httpExchange, String response) throws IOException {
        httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, 0);
        OutputStream body = httpExchange.getResponseBody();
        writeString(response, body);
        body.close();
    }

    /**
     * Converts the given InputStream to a String
     * @param is the InputStream to convert
     * @return the String representation of the InputStream
     * @throws IOException thrown if the Stream could not be converted
     */
    protected String readString(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        InputStreamReader sr = new InputStreamReader(is);
        char[] buf = new char[1024];
        int len;
        while ((len = sr.read(buf)) > 0) {
            sb.append(buf, 0, len);
        }
        return sb.toString();
    }

    /**
     * Writes the given string to the given OutputStream
     * @param str the string to write
     * @param os the OutputStream to write to
     * @throws IOException thrown if the string couldn't be written to the OutputStream
     */
    protected void writeString(String str, OutputStream os) throws IOException {
        OutputStreamWriter sw = new OutputStreamWriter(os);
        sw.write(str);
        sw.flush();
    }
}