package xyz.jmatt.services;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

import xyz.jmatt.APIKeys;
import xyz.jmatt.models.FCMMessage;

public class ClientNotifierService {
    public void notifyClients(String message) {
        try {
            System.out.println("notifying clients of new database changes...");
            URL url = new URL("https://fcm.googleapis.com/fcm/send");
            HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();

            httpURLConnection.setRequestProperty("Authorization", "key=" + APIKeys.getFCMKey());
            httpURLConnection.setRequestProperty("Content-Type", "application/json");
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setConnectTimeout(10000);

            String jsonData = new Gson().toJson(new FCMMessage(APIKeys.getClientID(), message), FCMMessage.class);

            OutputStream outputStream = httpURLConnection.getOutputStream();
            outputStream.write(jsonData.getBytes());

            httpURLConnection.connect();

            InputStream inputStream = httpURLConnection.getInputStream();
            Scanner s = new Scanner(inputStream).useDelimiter("\\A");
            String result = s.hasNext() ? s.next() : "";
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
