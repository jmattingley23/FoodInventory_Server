package xyz.jmatt.services;

import xyz.jmatt.APIKeys;

public class SetClientTokenService {
    public SetClientTokenService() {}

    public void setClientToken(String token) {
        APIKeys.setClientToken(token);
    }
}
