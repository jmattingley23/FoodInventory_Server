package xyz.jmatt.services;

import java.sql.SQLException;

import xyz.jmatt.daos.ItemDao;
import xyz.jmatt.daos.Transaction;
import xyz.jmatt.models.ItemModel;
import xyz.jmatt.results.SimpleResult;

public class UpdateItemService {
    public UpdateItemService() {}

    public SimpleResult updateItem(ItemModel itemModel) {
        SimpleResult result = null;

        System.out.println("Updating details for barcode [" + itemModel.getBarcode() + "]");
        Transaction transaction = null;
        try {
            transaction = new Transaction();
            ItemDao itemDao = new ItemDao(transaction);
            itemDao.updateItem(itemModel);

            transaction.commit();
            transaction.close();
            transaction = null;

            result = new SimpleResult("", false);
        } catch (SQLException e) {
            e.printStackTrace();
            result = new SimpleResult("ERROR: Could not connect to database", true);
        } finally {
            if(transaction != null) {
                transaction.rollback();
                transaction.close();
                transaction = null;
            }
        }
        return result;
    }
}
