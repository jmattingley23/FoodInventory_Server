package xyz.jmatt.services;

import java.sql.SQLException;

import xyz.jmatt.daos.ItemDao;
import xyz.jmatt.daos.Transaction;
import xyz.jmatt.models.ItemModel;

public class GetRecentItemsService {
    public GetRecentItemsService() {}

    public ItemModel getRecentItems() {
        System.out.println("updating recent items");
        Transaction transaction = null;
        ItemModel result = null;
        try {
            transaction = new Transaction();
            ItemDao itemDao = new ItemDao(transaction);

            result = itemDao.getRecentItems();

            transaction.commit();
            transaction.close();
            transaction = null;
        } catch (SQLException e) {
            e.printStackTrace();
            result = new ItemModel("ERROR: Could not connect to the database", true);
        } finally {
            if(transaction != null) {
                transaction.rollback();
                transaction.close();
                transaction = null;
            }
        }
        return result;
    }
}
