package xyz.jmatt.services;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;

import xyz.jmatt.daos.ItemDao;
import xyz.jmatt.daos.Transaction;
import xyz.jmatt.models.ItemModel;
import xyz.jmatt.models.UPCModel;
import xyz.jmatt.results.SimpleResult;

/**
 * Service to handle the scanning of a particular barcode[078742370002]
 */
public class ProcessItemService {
    public ProcessItemService() {}

    public SimpleResult processItem(String barcode) {
        SimpleResult result = null;
        //get the current scanner state
        GetScanService getScanService = new GetScanService();
        String state = getScanService.getScanState();

        System.out.println("Scanning barcode [" + barcode + "] " + state.toUpperCase());
        Transaction transaction = null;
        try {
            transaction = new Transaction();
            ItemDao itemDao = new ItemDao(transaction);

            //see if the item is already in the database
            if(itemDao.doesItemExist(barcode)) { //barcode was already found
                System.out.print("barcode " + barcode + " was already in the database, updating...");

                //if scanning in, increase quantity of item by 1
                if(state.equals("in")) {
                    itemDao.incrementItemQuantity(barcode);
                    transaction.commit();
                    transaction.close();
                    transaction = null;
                    System.out.println("added 1 item of " + barcode);
                    new ClientNotifierService().notifyClients("recents");
                    result = new SimpleResult("", false);
                } else { //decrease quantity by 1 for scanning out
                    itemDao.decrementItemQuantity(barcode);
                    transaction.commit();
                    transaction.close();
                    transaction = null;
                    System.out.println("removed 1 item of " + barcode);
                    new ClientNotifierService().notifyClients("recents");
                    result = new SimpleResult("", false);
                }
            } else { //new barcode
                System.out.println("new barcode " + barcode + " discovered");
                //make new model and push to database
                ItemModel model = gatherProductData(barcode);
                if(model == null) {
                    model = new ItemModel();
                    model.setBarcode(barcode);
                }
                if(model.isError()) {
                    return new SimpleResult(model.getMessage(), true);
                }
                itemDao.pushNewItem(model);

                transaction.commit();
                transaction.close();
                transaction = null;
                System.out.println("added to database");
                new ClientNotifierService().notifyClients("recents");
                result = new SimpleResult("", false);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            result = new SimpleResult("ERROR: Could not connect to database", true);
        } finally {
            if(transaction != null) {
                transaction.rollback();
                transaction.close();
                transaction = null;
            }
        }
        return result;
    }

    /**
     * Takes the given barcode and gathers usable product information using the upcitemdb API
     * @param barcode the barcode to get product info for
     * @return an ItemModel representation of the relevant information
     */
    private ItemModel gatherProductData(String barcode) {
        String originalBarcode = barcode;
        if(barcode.length() != 12) {
            barcode = convertUPCEToUPCA(barcode);
        }

        HttpURLConnection http = null;
        try {
            //setup a new HttpConnection
            URL url = new URL("https://api.upcitemdb.com/prod/trial/lookup?upc=" + barcode);
            http = (HttpURLConnection)url.openConnection();
            http.setConnectTimeout(10000);

            http.setRequestMethod("GET");
            http.setDoOutput(false);
            http.connect();

            //get the response body & read into a string
            StringBuilder stringBuilder = new StringBuilder();
            InputStreamReader streamReader = new InputStreamReader(http.getInputStream());
            char[] buf = new char[1024];
            int len;
            while ((len = streamReader.read(buf)) > 0) {
                stringBuilder.append(buf, 0, len);
            }
            String responseString = stringBuilder.toString();

            //convert the JSON string into a usable UPCModel object
            UPCModel upcModel = new Gson().fromJson(responseString, UPCModel.class);
            //proceed if the model was good
            if(upcModel.getCode().equals("OK") && upcModel.getItems().length > 0) {
                ItemModel itemModel = new ItemModel();
                itemModel.setBarcode(originalBarcode);
                //get the data from the UPCModel given by the API and put it into an ItemModel
                itemModel.setTitle(upcModel.getItems()[0].getTitle());
                if(upcModel.getItems()[0].getImages().length == 0) {
                    itemModel.setImage("");
                } else {
                    itemModel.setImage(upcModel.getItems()[0].getImages()[0]);
                }
                return itemModel;
            }
        } catch (MalformedURLException e) {
            System.err.println("ERROR: Bad URL");
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println("ERROR: Connection to UPC database failed...");
            try {
                if (http != null && http.getResponseCode() == 429) {
                    System.err.println("Scanning too many new items, please wait...");
                    new ClientNotifierService().notifyClients("ERROR: Scanning too many new items at once, please wait a minute.");
                } else {
                    System.err.println("ERROR: UPC database error... [BEGIN ERROR]");
                    e.printStackTrace();
                    System.err.println("[END ERROR]");
                }
            } catch (IOException er) {
                System.err.println("ERROR: Failed to get response code from UPC database... [BEGIN ERROR]");
                er.printStackTrace();
                System.err.println("[END ERROR]");
            }
            return new ItemModel("ERROR: UPC code was invalid", true);
        }
        return null;
    }

    private String convertUPCEToUPCA(String originalBarcode) {
        //first digit must be a 0 or a 1
        if(originalBarcode.length() != 8) {
            System.err.println("ERROR: barcode found was not 8 or 12 digits");
            return null;
        }
        if(originalBarcode.charAt(0) != '0' && originalBarcode.charAt(0) != '1') {
            return null;
        }
        char lastNonCheckDigit = originalBarcode.charAt(6);

        //https://stackoverflow.com/questions/31539005/how-to-convert-a-upc-e-barcode-to-a-upc-a-barcode
        StringBuilder sb = new StringBuilder();
        if(lastNonCheckDigit <= '2') {
            System.out.println("converting UPCE to UPCA using method 1");
            //first thing
            sb.append(originalBarcode.substring(0, 3));
            sb.append(originalBarcode.charAt(6));
            sb.append("0000");
            sb.append(originalBarcode.substring(3, 6));
            sb.append(originalBarcode.charAt(7));
        } else if(lastNonCheckDigit == '3') {
            System.out.println("converting UPCE to UPCA using method 2");
            //second thing
            sb.append(originalBarcode.substring(0, 4));
            sb.append("00000");
            sb.append(originalBarcode.substring(4, 6));
            sb.append(originalBarcode.charAt(7));
        } else if(lastNonCheckDigit == '4') {
            System.out.println("converting UPCE to UPCA using method 3");
            //third thing
            sb.append(originalBarcode.substring(0, 5));
            sb.append("00000");
            sb.append(originalBarcode.charAt(5));
            sb.append(originalBarcode.charAt(7));
        } else {
            System.out.println("converting UPCE to UPCA using method 4");
            //fourth thing
            sb.append(originalBarcode.substring(0, 6));
            sb.append("0000");
            sb.append(originalBarcode.charAt(6));
            sb.append(originalBarcode.charAt(7));
        }
        return sb.toString();
    }
}
