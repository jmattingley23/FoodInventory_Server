package xyz.jmatt.services;

import xyz.jmatt.models.ServerSingletonModel;

/**
 * Service to set the state of the scanner
 */
public class SetScanService {
    public SetScanService() {}

    public void setScanState(String state) {
        System.out.println("Setting scanner state to " + state.toUpperCase());
        ServerSingletonModel.getInstance().setScanState(state); //update in singleton
    }
}
