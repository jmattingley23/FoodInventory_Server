package xyz.jmatt.services;

import java.sql.SQLException;

import xyz.jmatt.daos.ItemDao;
import xyz.jmatt.daos.Transaction;
import xyz.jmatt.models.ItemModel;

public class GetAllItemsService {
    public GetAllItemsService() {}

    public ItemModel getItems() {
        System.out.println("getting all items from database");
        Transaction transaction = null;
        ItemModel result = null;
        try {
            transaction = new Transaction();
            ItemDao itemDao = new ItemDao(transaction);

            result = itemDao.getAllItems();

            transaction.commit();
            transaction.close();
            transaction = null;
        } catch (SQLException e) {
            result = new ItemModel("ERROR: Could not connect to the database", true);
        } finally {
            if(transaction != null) {
                transaction.rollback();
                transaction.close();
                transaction = null;
            }
        }
        return result;
    }
}
