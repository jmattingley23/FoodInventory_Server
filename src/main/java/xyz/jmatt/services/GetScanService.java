package xyz.jmatt.services;

import xyz.jmatt.models.ServerSingletonModel;

/**
 * Service to get the state of the scanner
 */
public class GetScanService {
    public GetScanService() {}

    public String getScanState() {
        return ServerSingletonModel.getInstance().getScanState(); //grab from singleton
    }
}
