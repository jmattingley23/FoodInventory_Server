package xyz.jmatt;

import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.net.InetSocketAddress;

import xyz.jmatt.handlers.GetAllItemsHandler;
import xyz.jmatt.handlers.GetRecentItemsHandler;
import xyz.jmatt.handlers.GetScanHandler;
import xyz.jmatt.handlers.ProcessItemHandler;
import xyz.jmatt.handlers.SetClientTokenHandler;
import xyz.jmatt.handlers.SetScanHandler;
import xyz.jmatt.handlers.UpdateItemHandler;

public class Server {
    private static final int MAX_WAITING_CONNECTIONS = 12;

    /**
     * Starts the HTTP server on the given port
     * @param portNumber the port to run the server on
     */
    private void run(int portNumber) {
        HttpServer server;
        System.out.print("Initializing HTTP Server on port " + portNumber + "...");
        try {
            server = HttpServer.create(new InetSocketAddress(portNumber), MAX_WAITING_CONNECTIONS);
        } catch (IOException e) {
            System.out.println();
            e.printStackTrace();
            return;
        }

        System.out.println("done.");
        server.setExecutor(null);
        System.out.println("Creating contexts...");

        System.out.print("\t/processItem/...");
        server.createContext("/processItem", new ProcessItemHandler());
        System.out.println("done.");

        System.out.print("\t/setScan/...");
        server.createContext("/setScan", new SetScanHandler());
        System.out.println("done.");

        System.out.print("\t/getScan/...");
        server.createContext("/getScan", new GetScanHandler());
        System.out.println("done");

        System.out.print("\t/getRecentItems/...");
        server.createContext("/getRecentItems", new GetRecentItemsHandler());
        System.out.println("done");

        System.out.print("\t/setClientToken/...");
        server.createContext("/setClientToken", new SetClientTokenHandler());
        System.out.println("done");

        System.out.print("\t/getAllItems/...");
        server.createContext("/getAllItems", new GetAllItemsHandler());
        System.out.println("done");

        System.out.print("\t/updateItem/...");
        server.createContext("/updateItem", new UpdateItemHandler());
        System.out.println("done");

        System.out.println("Contexts created.");
        System.out.println("Starting...");
        server.start();
    }

    /**
     * Main
     * @param args command line args USAGE: [port]
     */
    public static void main(String args[]) {
        System.out.print("Checking given port...");
        try {
            int portNumber = Integer.parseInt(args[0]);
            System.out.println("done.");
            new Server().run(portNumber);
        } catch (NumberFormatException e) {
            System.out.println("\nERROR: Given port was invalid.");
            System.out.println("Shutting down...");
            System.exit(0);
        }
    }
}
